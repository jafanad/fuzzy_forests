# Fuzzy Forests



## Generalities

R scripts and csv files repo for the fQCA of Forest2030 projects: 

- [ ] Based on Zuluaga's approach to fuzzyfying QCA (not forked).
- [ ] This extended approach invokes a (Bayesian) multivariate self-exciting Poisson process to calibrate membership values.
